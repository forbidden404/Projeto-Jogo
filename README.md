# Projeto-Jogo
[UFPE][if669ec][2015.2] Adventure-Text Game for 4th Programming Task - Course of Introduction to Programming in C

# Running
To run the game, you should follow the standard procedure:
```
git clone https://github.com/forbidden404/Projeto-Jogo.git
gcc main.c -o main 
./main
```
As this was a Programming Task related to my course, I haven't had the time or need to split it in several files, provide a Makefile, nor make sure it's indeed cross-platform.

The comments are in Portuguese for the time being, I will probably change that as soon as I get into my Summer Break.

# Gameplay
## Menu
The game starts with the initial menu asking for new game or load game. All commands need to be `[ENTER]` since the game runs through scanf/fgets.

To choose new game or load game, use `[w/s]` to move up and down and `[r]` to select the desired option.

If you choose new game, you will be asked to give the dimension of the map, which has to follow the following formula `5+(4*n)`.

After that you will be asked to write the whole map, then you will name your hero, write his/her stats and the follow by doing the same with each one of the four monsters.

This is the only difference from load game, since load game will take the previously written personagens.bin and mapa.txt as guide to start the game.

## Basic Movements and Rules
You `h` will be set in the middle of the map, each monster `m` in each quadrant and there will be trees `*` all over the map.

Each monster is confined within their quadrant and won't leave, also they won't be able to walk over a tree.

You can move with `[a/s/d/w] + [ENTER]`, being allowed to move all over the map. The only restriction is the border, you and the monsters are not allowed to walk there.

Each tree will give you 20 points and each turn will take 1 point and it can never be negative. Your HP will regenerate for 10% of your max HP, without being able to go over that.

Moving to a space where a monster is will start a battle.

## Battle
During a battle you will have two options, to run or to attack.

To run, just press `[r] + [ENTER]`. This will take half of your score and place you in the middle of the map again.

To attack, just press `[t] + [ENTER]`. This will trigger the attack() function and will add the amount of damage caused to a monster to your score.

## Game Over
The game will be over as soon as you hit 0 HP or all the monsters hit 0 HP.

By then you will be prompted to play again or quit, the instructions are pretty clear in this frame, trying again you take you to the first menu.

# To-Do List
- [ ] Split the code in several files
- [ ] Remove unnecessary comments that were made for the sake of understading to my TA
- [ ] Make English comments
- [ ] Use some 2D Library (probably SDL)
