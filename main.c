#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define swap(a, b) ((a)^=(b),(b)^=(a),(a)^=(b))

void *ec_malloc(unsigned int);

typedef struct player {
	char name[10];
	int atk;
	int def;
	int hp;
	int score;
	int row;
	int column;
} Player;

typedef struct monster {
	char name[10];
	int atk;
	int def;
	int hp;
	int row;
	int column;
} Monster;

void clear(); //func para limpar dependendo do sistema
void battle(Player *, Monster *, int, char **); //func para iniciar a batalha e deixar main() mais limpo
void attack(Player *, Monster *); //func de ataque que recebera player e monster
void run(Player *, int, char **); //func onde o player foge da batalha
void movement(Monster **, char **, int, int, Player *); //func para mover monstros
int move(int, int, int, int, int); //func pseudo-aleatoria para movimento dos monstros
int range(int, int, int, int); //func para ataque

int main(int argc, char const *argv[])
{
	FILE *fmenu = fopen("tela_inicial", "rb"); //fmenu sera binario para ler tudo de uma vez so
	FILE *fmap; //sera iniciado depois como "r" se load game ou "w+" se new game
	FILE *fcharacters; //sera iniciado depois como "rb" se load game ou "w+b" se new game
	FILE *fgame_over = fopen("game_over", "rb");

	//Variaveis declaradas no inicial e liberadas no final, inicializadas quando forem ser usadas
	char *input; //string que recebe menu
	char opt; //char para ler opt do jogador o jogo inteiro
	char menu_opt; //seleciona opt do menu
	char *name; //string para nome de player e monsters
	char **map; //ponteiro de ponteiro de char que recebera chars do mapa
	char c; //char para receber valores individuais do mapa
	int dim; //dimensao do mapa 
	int i, j; //contadores
	int turno; 
	int hp_max;
	int quit = 0;
	Player *player; //ponteiro para player para tornar acesso por memoria
	Monster **monsters; //ponteiro de ponteiro, pois cada monstro sera um ponteiro que nem player

	while(quit == 0) {
		//acharemos o tamanho total do fmenu para passar tudo para uma string
		long menu_size;
		fseek(fmenu, 0, SEEK_END); //seta cursor para final do arquivo
		menu_size = ftell(fmenu); //retorna o tamanho do inicio do arquivo ate o cursor
		rewind(fmenu); //retorna cursor pro começo do arquivo

		input = (char *) ec_malloc((menu_size + 1) * sizeof(char));
		fread(input, sizeof(char), menu_size, fmenu);
		input[menu_size] = '\0';

		menu_opt = 'n'; //new game por padrao
		clear();
		printf("%s", input);
		scanf("%c%*c", &opt); //%*c ignora \n

		while (opt != 'r') {
			switch (opt) {
				case 's':
				case 'S':
					clear();
					if (menu_opt == 'n') //muda pos de '>'
						swap(input[1662], input[1826]);
					menu_opt = 'l';
					printf("%s", input);
					scanf("%c%*c", &opt);
					break;
				case 'w':
				case 'W':
					clear();
					if (menu_opt == 'l') //muda pos de '>'
						swap(input[1662], input[1826]);
					menu_opt = 'n';
					printf("%s", input);
					scanf("%c%*c", &opt);
					break;
				case 'r':
				case 'R':
					break;
				default:
					clear();
					printf("Digite uma opt valida [s/w]\n");
					scanf("%c%*c", &opt);
					break;
			}
		}

		//inicializa player, monster e strings necessarias
		player = (Player *) ec_malloc(sizeof(Player));
		memset(player, 0, sizeof(Player)); //memset o ponteiro para 0 para evitar lixo de memoria
		monsters = (Monster **) ec_malloc(sizeof(Monster *) * 4);
		for (i = 0; i < 4; ++i) {
			monsters[i] = (Monster *) ec_malloc(sizeof(Monster));
			memset(monsters[i], 0, sizeof(Monster));
		}

		name = (char *) ec_malloc(10 * sizeof(char));

		//new game
		if (menu_opt == 'n') {
			fmap = fopen("mapa.txt", "w+");
			printf("Digite n para as dimensões do mapa: [5+(4*n)x5(4*n)]\n");
			scanf("%d%*c", &dim);
			dim = 5 + (4 * dim);
			map = (char **) ec_malloc(dim * sizeof(char *));
			for (i = 0; i < dim; ++i)
				map[i] = (char *) ec_malloc(dim * sizeof(char));
			printf("Digite o mapa: \n");
			for (i = 0; i < dim; ++i) {
				for (j = 0; j < dim; ++j) {
					scanf("%c", &c);
					if (c != '\n')
						map[i][j] = c;
					else { //se c for \n, ler proximo valor
						scanf("%c", &c);
						map[i][j] = c;
					}
				}
			}

			fprintf(fmap, "%d\n", dim); //começar a escrever em fmap
			for (i = 0; i < dim; ++i) {
				for (j = 0; j < dim; ++j) {
					fprintf(fmap, "%c", map[i][j]);
				}
				fprintf(fmap, "\n");
			}
			getchar(); //limpa \n sem usar flush(stdin), por mais que funcione, não é aconselhado usar undefined behavior
			printf("Digite o nome do heroi: \n");
			fgets(name, 10, stdin);
			strtok(name, "\n"); //fgets pegará o \n da string, strtok vai retirar
			strcpy(player->name, name); //copia nome;
			printf("Digite os atributos do heroi: [atk], [def] e [hp]\n");
			scanf("%d %d %d%*c", &(player->atk), &(player->def), &(player->hp));

			for (i = 0; i < 4; ++i) {
				printf("Digite o nome do monstro %d: \n", i + 1);
				fgets(name, 10, stdin);
				strtok(name, "\n"); //fgets pegará o \n da string, strtok vai retirar
				strcpy(monsters[i]->name, name); //copia nome;
				printf("Digite os atributos do monstro %d: [atk], [def] e [hp]\n", i + 1);
				scanf("%d %d %d%*c", &(monsters[i]->atk), &(monsters[i]->def), &(monsters[i]->hp));
			}

			//hora de escrever essas infos em fcharacters
			fcharacters = fopen("personagens.bin", "w+b");
			fwrite(player, sizeof(Player), 1, fcharacters);
			fwrite(monsters[0], sizeof(Monster), 1, fcharacters);
			fwrite(monsters[1], sizeof(Monster), 1, fcharacters);
			fwrite(monsters[2], sizeof(Monster), 1, fcharacters);
			fwrite(monsters[3], sizeof(Monster), 1, fcharacters);
		} else {
			fmap = fopen("mapa.txt", "r");
			fcharacters = fopen("personagens.bin", "rb");

			//ler info de fcharacters e fmap
			fread(player, sizeof(Player), 1, fcharacters);
			fread(monsters[0], sizeof(Monster), 1, fcharacters);
			fread(monsters[1], sizeof(Monster), 1, fcharacters);
			fread(monsters[2], sizeof(Monster), 1, fcharacters);
			fread(monsters[3], sizeof(Monster), 1, fcharacters);

			fgets(input, 10, fmap);
			strtok(input, "\n");
			dim = atoi(input);

			map = (char **) ec_malloc(sizeof(char *) * dim);
			for (i = 0; i < dim; ++i)
				map[i] = (char *) ec_malloc(sizeof(char) * dim);

			for (i = 0; i < dim; ++i) {
				for (j = 0; j < dim; ++j) {
					fscanf(fmap, "%c", &c);
					if (c != '\n')
						map[i][j] = c;
					else {
						fscanf(fmap, "%c", &c);
						map[i][j] = c;
					}
				}
			}
		}

		//Setar pos inicial de player e monsters
		player->row = dim/2;
		player->column  = dim/2;
		map[player->row][player->column] = 'h';

		monsters[0]->row = dim/4;
		monsters[0]->column = dim/4;
		map[monsters[0]->row][monsters[0]->column] = 'm';

		monsters[1]->row = dim/4;
		monsters[1]->column = (3*dim)/4;
		map[monsters[1]->row][monsters[1]->column] = 'm';

		monsters[2]->row = (3*dim)/4;
		monsters[2]->column = dim/4;
		map[monsters[2]->row][monsters[2]->column] = 'm';

		monsters[3]->row = (dim/4)+(dim/2);
		monsters[3]->column = (dim/4)+(dim/2);
		map[monsters[3]->row][monsters[3]->column] = 'm';

		//inicia variaveis para jogo
		turno = 0;
		hp_max = player->hp;
		player->score = 0;

		while (1) { //while sempre sera true e irá parar com break
			clear();
			for (i = 0; i < dim; ++i) {
				for (j = 0; j < dim; ++j) {
					printf("%c", map[i][j]);
				}
				printf("\n");
			}
			printf("HP: %d\n", player->hp);
			printf("SCORE: %d\n", player->score);
			printf("TURNO: %d\n", turno);
			scanf("%c%*c", &opt);

			switch (opt) {
				case 'a':
				case 'A':
					if (player->column - 1 > 0) {
						if (map[player->row][player->column - 1] == '*') {
							map[player->row][player->column] = ' ';
							player->column -= 1;
							player->score += 20;
							map[player->row][player->column] = 'h';
						} else if (map[player->row][player->column - 1] == ' ') {
							map[player->row][player->column] = ' ';
							player->column -= 1;
							map[player->row][player->column] = 'h';
						} else if (map[player->row][player->column - 1] == 'm') {
							for (i = 0; i < 4; ++i) {
								if (monsters[i]->row == player->row && monsters[i]->column == player->column - 1) {
									battle(player, monsters[i], dim, map);
									break;
								}
							}
						}
					}
					break;
				case 's':
				case 'S':
					if (player->row + 1 < dim - 1) {
						if (map[player->row + 1][player->column] == '*') {
							map[player->row][player->column] = ' ';
							player->row += 1;
							player->score += 20;
							map[player->row][player->column] = 'h';
						} else if (map[player->row + 1][player->column] == ' ') {
							map[player->row][player->column] = ' ';
							player->row += 1;
							map[player->row][player->column] = 'h';
						} else if (map[player->row + 1][player->column] == 'm') {
							for (i = 0; i < 4; ++i) {
								if (monsters[i]->row == player->row + 1 && monsters[i]->column == player->column) {
									battle(player, monsters[i], dim, map);
									break;
								}
							}
						}
					}
					break;
				case 'd':
				case 'D':
					if (player->column + 1 < dim - 1) {
						if (map[player->row][player->column + 1] == '*') {
							map[player->row][player->column] = ' ';
							player->column += 1;
							player->score += 20;
							map[player->row][player->column] = 'h';
						} else if (map[player->row][player->column + 1] == ' ') {
							map[player->row][player->column] = ' ';
							player->column += 1;
							map[player->row][player->column] = 'h';
						} else if (map[player->row][player->column + 1] == 'm') {
							for (i = 0; i < 4; ++i) {
								if (monsters[i]->row == player->row && monsters[i]->column == player->column + 1) {
									battle(player, monsters[i], dim, map);
									break;
								}
							}
						}
					}
					break;
				case 'w':
				case 'W':
					if (player->row - 1 > 0) {
						if (map[player->row - 1][player->column] == '*') {
							map[player->row][player->column] = ' ';
							player->row -= 1;
							player->score += 20;
							map[player->row][player->column] = 'h';
						} else if (map[player->row - 1][player->column] == ' ') {
							map[player->row][player->column] = ' ';
							player->row -= 1;
							map[player->row][player->column] = 'h';
						} else if (map[player->row - 1][player->column] == 'm') {
							for (i = 0; i < 4; ++i) {
								if (monsters[i]->row == player->row - 1 && monsters[i]->column == player->column) {
									battle(player, monsters[i], dim, map);
									break;
								}
							}
						}
					}
					break;
				default:
					printf("Entrada invalida: [a/s/w/d]\n");
					break;
			}

			clear();
			if (player->hp <= 0 || (monsters[0]->hp <= 0 && monsters[1]->hp <= 0 && monsters[2]->hp <= 0 && monsters[3]->hp <= 0))
				break;

			movement(monsters, map, dim, turno, player);

			turno++;
			if (player->score >= 1)
				player->score -= 1;
			else
				player->score = 0;

			player->hp += (hp_max/10);
			if (player->hp > hp_max)
				player->hp = hp_max;

		}

		fseek(fgame_over, 0, SEEK_END); //seta cursor para final do arquivo
		menu_size = ftell(fgame_over); //retorna o tamanho do inicio do arquivo ate o cursor
		rewind(fgame_over); //retorna cursor pro começo do arquivo

		input = (char *) realloc(input, (menu_size + 1) * sizeof(char));
		fread(input, sizeof(char), menu_size, fgame_over);
		input[menu_size] = '\0';

		printf("%s", input);	
		scanf("%c%*c", &opt);
		switch (opt) {
			case 'r':
			case 'R':
				quit = 1;
				break;
			case 't':
			case 'T':
				quit = 0;
				break;
			default:
				printf("Entrada invalida, saindo...\n");
				quit = 1;

		}
	}

	fclose(fmenu);
	fclose(fmap);
	fclose(fcharacters);
	fclose(fgame_over);

	free(input);
	free(name);
	for (i = 0; i < dim; ++i) //libera memoria de cada ponteiro de ponteiro
		free(map[i]);
	free(map);
	free(player);
	for (i = 0; i < 4; ++i) //libera memoria de cada monstro
		free(monsters[i]);
	free(monsters);

	return 0;
}

void clear()
{
#if __linux__
	system("clear");
#elif _WIN32
	system("cls");
#endif
}

void *ec_malloc(unsigned int size)
{
	void *ptr;
	ptr = malloc(size);
	if (ptr == NULL) {
		fprintf(stderr, "Erro ao alocar memoria\n");
		exit(1);
	}

	return ptr;
}

void battle(Player *player, Monster *monster, int dim, char **map)
{
	FILE *ffight_frame = fopen("fight_frame", "rb");

	char *input;
	char opt;

	int fight = 1;

	long fsize;
	fseek(ffight_frame, 0, SEEK_END);
	fsize = ftell(ffight_frame);
	rewind(ffight_frame);

	clear();
	input = (char *) ec_malloc(sizeof(char) * (fsize+1));
	input[fsize] = '\0';
	fread(input, sizeof(char), fsize, ffight_frame);

	while (fight) {
		printf("%s", input);

		printf("HP: %d\n", player->hp);
		printf("ATK: %d\n", player->atk);
		printf("DEF: %d\n", player->def);

		printf("\n");

		printf("HP: %d\n", monster->hp);
		printf("ATK: %d\n", monster->atk);
		printf("DEF: %d\n", monster->def);

		scanf("%c%*c", &opt);

		switch (opt) {
			case 'r':
			case 'R':
				run(player, dim, map);
				fight = 0;
				break;
			case 't':
			case 'T':
				attack(player, monster);
				if (monster->hp > 0) {
					player->hp -= monster->atk + range(monster->hp, monster->atk, player->hp, player->atk) - player->def;
				}
				break;
			default:
				printf("Opt invalida: [r/t]\n");
				break;
		}

		if (opt == 'r' || opt == 'R')
			fight = 0;
		if (((player->hp)*(monster->hp)) <= 0)
			fight = 0;
	}

	if (monster->hp <= 0) {
		map[player->row][player->column] = ' ';
		map[monster->row][monster->column] = 'h';
		player->row = monster->row;
		player->column = monster->column;
		monster->row = 0;
		monster->column = 0;
	}

	fclose(ffight_frame);

	free(input);
}

int move(int w, int x, int y, int z, int e)
{
	if ((w*y*z*y) == 0)
		return 0;
	return (((w+x+e)*(y/z)) + move(z-1, w-1, x-1, y-1, e)) % 5;
}

int range(int a, int b, int c, int d) 
{
	if ((a*b*c*d) == 0)
		return 0;
	return (((a+b) * c / (d + 1)) + range(d-10, a-10, b-10, c-10)) % 30;
}

void run(Player *player, int dim, char **map)
{
	map[player->row][player->column] = ' ';
	player->score /= 2;
	player->row = dim/2;
	player->column = dim/2;
	map[player->row][player->column] = 'h';
}

void attack(Player *player, Monster *monster)
{	
	monster->hp -= player->atk + range(player->hp, player->atk, monster->hp, monster->atk) - monster->def;
	player->score += (player->atk + range(player->hp, player->atk, monster->hp, monster->atk) - monster->def) > 0 
		? player->atk + range(player->hp, player->atk, monster->hp, monster->atk) - monster->def 
		: (player->atk + range(player->hp, player->atk, monster->hp, monster->atk) - monster->def)* -1;
}

void movement(Monster **monsters, char **map, int dim, int turno, Player *player)
{
	int i;
	for (i = 0; i < 4; ++i) {
		if (monsters[i]->hp >= 0)
			switch(move(player->row, player->column, monsters[i]->row, monsters[i]->column, turno)) {
				case 0:
					switch (i) {
						case 0:
							if (monsters[i]->column + 1 < (dim - 1)/2) {
								if (map[monsters[i]->row][monsters[i]->column + 1] == ' ') {
									map[monsters[i]->row][monsters[i]->column] = ' ';
									map[monsters[i]->row][monsters[i]->column + 1] = 'm';
									monsters[i]->column += 1;
								}
							}
							break;
						case 1:
							if (monsters[i]->column + 1 < dim - 1) {
								if (map[monsters[i]->row][monsters[i]->column + 1] == ' ') {
									map[monsters[i]->row][monsters[i]->column] = ' ';
									map[monsters[i]->row][monsters[i]->column + 1] = 'm';
									monsters[i]->column += 1;
								}
							}
							break;
						case 2:
							if (monsters[i]->column + 1 < (dim - 1)/2) {
								if (map[monsters[i]->row][monsters[i]->column + 1] == ' ') {
									map[monsters[i]->row][monsters[i]->column] = ' ';
									map[monsters[i]->row][monsters[i]->column + 1] = 'm';
									monsters[i]->column += 1;
								}
							}
							break;
						case 3:
							if (monsters[i]->column + 1 < dim - 1) {
								if (map[monsters[i]->row][monsters[i]->column + 1] == ' ') {
									map[monsters[i]->row][monsters[i]->column] = ' ';
									map[monsters[i]->row][monsters[i]->column + 1] = 'm';
									monsters[i]->column += 1;
								}
							}
							break;
					}
					break;
				case 1:	
					switch (i) {
						case 0:
							if (monsters[i]->row - 1 > 0) {
								if (map[monsters[i]->row - 1][monsters[i]->column] == ' ') {
									map[monsters[i]->row][monsters[i]->column] = ' ';
									map[monsters[i]->row - 1][monsters[i]->column] = 'm';
									monsters[i]->row -= 1;
								}
							}
							break;
						case 1:
							if (monsters[i]->row - 1 > 0) {
								if (map[monsters[i]->row - 1][monsters[i]->column] == ' ') {
									map[monsters[i]->row][monsters[i]->column] = ' ';
									map[monsters[i]->row - 1][monsters[i]->column] = 'm';
									monsters[i]->row -= 1;
								}
							}
							break;
						case 2:
							if (monsters[i]->row - 1 > (dim - 1)/2) {
								if (map[monsters[i]->row - 1][monsters[i]->column] == ' ') {
									map[monsters[i]->row][monsters[i]->column] = ' ';
									map[monsters[i]->row - 1][monsters[i]->column] = 'm';
									monsters[i]->row -= 1;
								}
							}
							break;
						case 3:
							if (monsters[i]->row - 1 > (dim - 1)/2) {
								if (map[monsters[i]->row - 1][monsters[i]->column] == ' ') {
									map[monsters[i]->row][monsters[i]->column] = ' ';
									map[monsters[i]->row - 1][monsters[i]->column] = 'm';
									monsters[i]->row -= 1;
								}
							}
							break;
					}
					break;
				case 2:
					switch (i) {
						case 0:
							if (monsters[i]->column - 1 > 0) {
								if (map[monsters[i]->row][monsters[i]->column - 1] == ' ') {
									map[monsters[i]->row][monsters[i]->column] = ' ';
									map[monsters[i]->row][monsters[i]->column - 1] = 'm';
									monsters[i]->column -= 1;
								}
							}
							break;
						case 1:
							if (monsters[i]->column - 1 > (dim - 1) / 2) {
								if (map[monsters[i]->row][monsters[i]->column - 1] == ' ') {
									map[monsters[i]->row][monsters[i]->column] = ' ';
									map[monsters[i]->row][monsters[i]->column - 1] = 'm';
									monsters[i]->column -= 1;
								}
							}
							break;
						case 2:
							if (monsters[i]->column - 1 > 0) {
								if (map[monsters[i]->row][monsters[i]->column - 1] == ' ') {
									map[monsters[i]->row][monsters[i]->column] = ' ';
									map[monsters[i]->row][monsters[i]->column - 1] = 'm';
									monsters[i]->column -= 1;
								}
							}
							break;
						case 3:
							if (monsters[i]->column - 1 > (dim - 1)/2) {
								if (map[monsters[i]->row][monsters[i]->column - 1] == ' ') {
									map[monsters[i]->row][monsters[i]->column] = ' ';
									map[monsters[i]->row][monsters[i]->column - 1] = 'm';
									monsters[i]->column -= 1;
								}
							}
							break;
					}
					break;
				case 3:
					switch (i) {
						case 0:
							if (monsters[i]->row + 1 < (dim - 1)/2) {
								if (map[monsters[i]->row + 1][monsters[i]->column] == ' ') {
									map[monsters[i]->row][monsters[i]->column] = ' ';
									map[monsters[i]->row + 1][monsters[i]->column] = 'm';
									monsters[i]->row += 1;
								}
							}
							break;
						case 1:
							if (monsters[i]->row + 1 < (dim - 1)/2) {
								if (map[monsters[i]->row + 1][monsters[i]->column] == ' ') {
									map[monsters[i]->row][monsters[i]->column] = ' ';
									map[monsters[i]->row + 1][monsters[i]->column] = 'm';
									monsters[i]->row += 1;
								}
							}
							break;
						case 2:
							if (monsters[i]->row + 1 < dim - 1) {
								if (map[monsters[i]->row + 1][monsters[i]->column] == ' ') {
									map[monsters[i]->row][monsters[i]->column] = ' ';
									map[monsters[i]->row + 1][monsters[i]->column] = 'm';
									monsters[i]->row += 1;
								}
							}
							break;
						case 3:
							if (monsters[i]->row + 1 < dim - 1) {
								if (map[monsters[i]->row + 1][monsters[i]->column] == ' ') {
									map[monsters[i]->row][monsters[i]->column] = ' ';
									map[monsters[i]->row + 1][monsters[i]->column] = 'm';
									monsters[i]->row += 1;
								}
							}
							break;
					}
					break;
			}
	}
}
